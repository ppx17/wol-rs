use std::fmt;
use std::num::ParseIntError;
use actix_web::{App, get, HttpResponse, HttpServer, Responder, web};
use actix_web::error::{ErrorBadRequest, ErrorInternalServerError};
use regex::Regex;

#[get("/wol/{mac}")]
async fn wol(mac: web::Path<String>) -> impl Responder {
    let mac_regex = Regex::new(r"^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$").unwrap();
    if !mac_regex.is_match(&mac) {
        eprintln!("Invalid MAC address: {}", mac);
        return Err(ErrorBadRequest(format!("Invalid MAC address: {}", mac)));
    }

    let mac_address = match mac_to_u8(&mac) {
        Ok(mac_address) => mac_address,
        Err(e) => return Err(ErrorBadRequest(format!("Invalid MAC address: {}", e))),
    };

    let magic_packet = wake_on_lan::MagicPacket::new(&mac_address);

    let result = magic_packet.send();

    match result {
        Ok(_) => {
            let msg = format!("Magic packet sent successfully to {}", u8_to_mac(&mac_address));
            println!("{}", msg);
            return Ok(HttpResponse::Ok().body(msg))
        },
        Err(e) => Err(ErrorInternalServerError(format!("Error sending magic packet: {}", e))),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let listen_addr = std::env::var("LISTEN_ADDR").unwrap_or_else(|_| String::from("0.0.0.0"));
    let port = std::env::var("PORT").unwrap_or_else(|_| String::from("9123"));
    let port: u16 = port.parse().unwrap_or_else(|_| {
        eprintln!("Warning: PORT is not a valid number, defaulting to 9123");
        9123
    });

    println!("Starting server at {}:{}", listen_addr, port);

    HttpServer::new(|| {
        App::new().service(wol)
    })
        .bind((listen_addr, port))?
        .run()
        .await?;

    println!("Server started successfully");

    Ok(())
}

#[derive(Debug)]
pub struct InvalidMacAddressError {
    pub details: String,
}

impl InvalidMacAddressError {
    pub fn new(msg: &str) -> InvalidMacAddressError {
        InvalidMacAddressError{details: msg.to_string()}
    }
}

impl fmt::Display for InvalidMacAddressError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl std::error::Error for InvalidMacAddressError {
    fn description(&self) -> &str {
        &self.details
    }
}

impl From<ParseIntError> for InvalidMacAddressError {
    fn from(_: ParseIntError) -> Self {
        InvalidMacAddressError::new("Invalid MAC address: Mac address must be a valid hex string")
    }
}

fn mac_to_u8(mac: &str) -> Result<[u8; 6], InvalidMacAddressError> {
    let parts: Vec<&str> = mac.split(|c| c == ':' || c == '-').collect();
    if parts.len() != 6 {
        return Err(InvalidMacAddressError::new("Invalid MAC address: Mac address must have 6 parts"));
    }
    let mut mac_address: [u8; 6] = [0; 6];
    for (i, part) in parts.iter().enumerate() {
        mac_address[i] = u8::from_str_radix(part, 16)?;
    }
    Ok(mac_address)
}

fn u8_to_mac(mac: &[u8; 6]) -> String {
    mac.iter().map(|b| format!("{:02x}", b)).collect::<Vec<String>>().join(":")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_mac_to_u8_valid() {
        let mac = "2c:44:fd:00:00:00";
        let expected = [0x2c, 0x44, 0xfd, 0x00, 0x00, 0x00];
        let result = mac_to_u8(mac).unwrap();
        assert_eq!(result, expected);
    }

    #[test]
    fn test_mac_to_u8_invalid_format() {
        let mac = "2c-44-fd-00-00-00";
        let expected = [0x2c, 0x44, 0xfd, 0x00, 0x00, 0x00];
        let result = mac_to_u8(mac).unwrap();
        assert_eq!(result, expected);
    }

    #[test]
    fn test_mac_to_u8_invalid_separator() {
        let mac = "2c#44#fd#00#00#00";
        let result = mac_to_u8(mac);
        assert!(result.is_err());
    }

    #[test]
    fn test_mac_to_u8_invalid_length() {
        let mac = "2c:44:fd:00:00";
        let result = mac_to_u8(mac);
        assert!(result.is_err());
    }
}