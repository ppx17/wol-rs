FROM alpine

COPY target/release/wol-rs /usr/local/bin/wol-rs

CMD ["wol-rs"]